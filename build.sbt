name := "spark-training"
version := "1.0"
scalaVersion := "2.11.12"


libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" % "scala-logging_2.11" % "3.9.0",
  "org.apache.spark" % "spark-sql_2.11" % "2.2.2",
  "com.google.cloud.bigdataoss" % "gcs-connector" % "1.6.2-hadoop2",
  "com.google.cloud.bigdataoss" % "bigquery-connector" % "0.10.11-hadoop2",
  "joda-time" % "joda-time" % "2.10.1",
  "org.scalatest" % "scalatest_2.11" % "3.0.7" % Test,
  "pl.datumo" % "spark-bigquery_2.11" % "0.1.1" from "https://storage.googleapis.com/datumo/jars/spark-bigquery/master/spark-bigquery_2.11-0.1.1.jar"
)
