package pl.datumo.sages

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}

object EnricherUtils {

  val Unknown: String = "Unknown"
  val DateFormat: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")

  def getMillisFromDateTime(dateTime: Option[DateTime]): Option[Long] = {
    ???
  }
  def addLeadingZeroIfMissing(value: String): String = {
    ???
  }
  def getYearAsString(dateTime: Option[DateTime]): String = {
    ???
  }
  def getMonthAsString(dateTime: Option[DateTime]): String = {
    ???
  }
  def getDayAsString(dateTime: Option[DateTime]): String = {
    ???
  }
  def getQuarterAsString(dateTime: Option[DateTime]): String = {
    ???
  }
  def calculateAverage(dividend: Option[Double], divider: Option[Long]): Option[Double] = {
    ???
  }
}
