package pl.datumo.sages.headers

case class SalesEnriched(
                          date: Option[String],
                          timestampMillis: Option[Long],
                          year: String,
                          month: String,
                          day: String,
                          quarter: String,
                          retailerCountry: String,
                          orderMethodType: String,
                          retailerType: String,
                          productLine: String,
                          productType: String,
                          productId: Option[Long],
                          revenue: Option[Double],
                          quantity: Option[Long],
                          grossMargin: Option[Double],
                          grossMarginPerProduct: Option[Double],
                          revenuePerProduct: Option[Double]
                        )
