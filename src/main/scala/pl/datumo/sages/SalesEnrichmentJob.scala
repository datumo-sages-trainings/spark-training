package pl.datumo.sages

import org.apache.spark.sql.{SaveMode, SparkSession}
import pl.datumo.integration.spark.bigquery._

object SalesEnrichmentJob {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .config("spark.master", "local[*]")
      .config("spark.hadoop.fs.gs.project.id", "datumo-sages-training")
      .config("spark.hadoop.bq.staging_dataset.location", "EU")
      .config("spark.hadoop.mapred.bq.project.id", "datumo-sages-training")
      .config("spark.hadoop.mapred.bq.gcs.bucket", "datumo-sages-training_temp")
      .config("google.cloud.auth.service.account.enable", "true")
      .config("google.cloud.auth.service.account.json.keyfile", "service_key/service_key.json")
      .appName("DatumoSagesTraining")
      .getOrCreate()

    val query  = "SELECT * FROM sales.sales"

    import spark.implicits._

    val sales = spark.sqlContext.bigQuerySelect(query).repartition(5)

    sales.show(10)

    //create your own bucket
    // salesEnriched.write.mode(SaveMode.Overwrite).option("header", "true").csv("gs://...")

    spark.stop()
  }
}